defmodule HunterGatherer.ProcessorTest do
  use ExUnit.Case, async: true

  alias HunterGatherer.Processor

  @html_with_links """
  <html>
  <body>
  <a href="/relative">rel</a>
  <a href="/relative#with-anchor">rel-anch</a>
  <a href="#">dummy</a>
  <a href="javascript:alert('test')">test</a>
  <a href="https://google.com">google</a>
  <a href="mailto:test@test.com">mail me</a>
  </body>
  </html>
  """

  describe "get_links/3" do
    test "return empty list for external url" do
      config = %{base_url: "https://test.com"}
      assert Processor.get_links("http://localhost", @html_with_links, config) == []
    end

    # TODO: strip anchors
    test "return links when url is internal" do
      config = %{base_url: "http://localhost"}

      assert Processor.get_links("http://localhost", @html_with_links, config) == [
               "http://localhost/relative",
               "http://localhost/relative#with-anchor",
               "http://localhost#",
               "https://google.com"
             ]
    end

    test "return empty list for internal link and html without links" do
      html = "<html><body><p>empty</p></body></html>"
      config = %{base_url: "http://localhost"}
      assert Processor.get_links("http://localhost", html, config) == []
    end
  end
end
