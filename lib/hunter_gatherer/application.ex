defmodule HunterGatherer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: HunterGatherer.Worker.start_link(arg)
      # {HunterGatherer.Worker, arg}
      # {HunterGatherer.Job, :ok},
      # {HunterGatherer.UrlQueue, :ok},
      # {HunterGatherer.Fetcher, :ok}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: HunterGatherer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
