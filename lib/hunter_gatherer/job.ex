defmodule HunterGatherer.Job do
  @doc """
  This is the main module coordinating whole scraping process. It spawns
  stages and stages should only talk to it. It also holds the initial configuration
  of the job as well as the status of the processing.
  """

  use GenServer

  alias __MODULE__
  alias HunterGatherer.JobState
  alias HunterGatherer.Reporter
  alias HunterGatherer.Stage.Extractor
  alias HunterGatherer.Stage.Fetcher
  alias HunterGatherer.Stage.UrlQueue

  def start(url) do
    # TODO: parameterize namespace from params
    namespace = "default"

    config = %{
      job_name: String.to_atom("#{namespace}.job"),
      url_queue_name: String.to_atom("#{namespace}.url_queue"),
      fetcher_name: String.to_atom("#{namespace}.fetcher"),
      extractor_name: String.to_atom("#{namespace}.extractor"),
      base_url: url,
      limit: 200
    }

    children = [
      {Job, config},
      {UrlQueue, config},
      {Fetcher, config},
      {Extractor, config}
    ]

    {:ok, _pid} =
      Supervisor.start_link(children,
        strategy: :one_for_one,
        name: String.to_atom("#{namespace}.supervisor")
      )

    UrlQueue.add_urls([url], config.url_queue_name)
  end

  def start_link(config) do
    GenServer.start_link(__MODULE__, config, name: config.job_name)
  end

  def process_success(url, found_links, name) do
    GenServer.call(name, {:success, url, found_links})
  end

  def process_error(url, reason, name) do
    GenServer.call(name, {:error, url, reason})
  end

  # callbacks

  @impl true
  def init(config) do
    Process.send_after(self(), :finished?, 1000)
    {:ok, JobState.new(config)}
  end

  @impl true
  def handle_call({:success, url, found_links}, _from, state) do
    state = JobState.add_good(state, url)
    state = Enum.reduce(found_links, state, fn link, acc -> JobState.mark_hit(acc, link) end)
    to_queue = JobState.urls_to_queue(state, found_links)

    state =
      state
      |> JobState.queue_urls(to_queue)

    UrlQueue.add_urls(to_queue, state.config.url_queue_name)

    {:reply, to_queue, state}
  end

  @impl true
  def handle_call({:error, url, reason}, _from, state) do
    state = JobState.add_bad(state, url, reason)
    {:reply, [], state}
  end

  @impl true
  def handle_info(:finished?, state) do
    case JobState.finished?(state) do
      true -> Reporter.render(state, format: :cli) |> IO.puts()
      false -> Process.send_after(self(), :finished?, 1000)
    end

    {:noreply, state}
  end
end
