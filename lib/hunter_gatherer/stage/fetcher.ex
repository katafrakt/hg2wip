defmodule HunterGatherer.Stage.Fetcher do
  use GenStage

  alias HunterGatherer.Processor

  def start_link(config) do
    GenStage.start_link(__MODULE__, config, name: config.fetcher_name)
  end

  def init(config) do
    {:producer_consumer, config, subscribe_to: [{config.url_queue_name, max_demand: 1}]}
  end

  def handle_events([url], _from, config) do
    case Processor.process_url(url, config) do
      {:ok, body} ->
        {:noreply, [{:ok, url, body}], config}

      {:error, reason} ->
        IO.puts("ERROR #{inspect(reason)}")
        {:noreply, [{:error, url, reason}], config}
    end
  end
end
