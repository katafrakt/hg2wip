defmodule HunterGatherer.Stage.UrlQueue do
  use GenStage

  def start_link(config) do
    GenStage.start_link(__MODULE__, :ok, name: config.url_queue_name)
  end

  def add_urls(urls, name) do
    GenStage.cast(name, {:add_urls, urls})
  end

  # callbacks

  @impl true
  def init(:ok) do
    {:producer, {:queue.new(), 0}}
  end

  @impl true
  def handle_cast({:add_urls, urls}, {queue, pending_demand}) do
    queue = Enum.reduce(urls, queue, fn x, acc -> :queue.in(x, acc) end)
    dispatch_events(queue, pending_demand, [])
  end

  @impl true
  def handle_demand(incoming_demand, {queue, pending_demand}) do
    dispatch_events(queue, incoming_demand + pending_demand, [])
  end

  defp dispatch_events(queue, 0, events) do
    {:noreply, Enum.reverse(events), {queue, 0}}
  end

  defp dispatch_events(queue, demand, events) do
    case :queue.out(queue) do
      {{:value, event}, queue} ->
        dispatch_events(queue, demand - 1, [event | events])

      {:empty, queue} ->
        {:noreply, Enum.reverse(events), {queue, demand}}
    end
  end
end
