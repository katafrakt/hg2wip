defmodule HunterGatherer.Stage.Extractor do
  use GenStage

  alias HunterGatherer.Job
  alias HunterGatherer.Processor

  def start_link(config) do
    GenStage.start_link(__MODULE__, config, name: config.extractor_name)
  end

  def init(config) do
    {:consumer, config, subscribe_to: [{config.fetcher_name, max_demand: 10}]}
  end

  def handle_events(htmls, _from, state) do
    Enum.each(htmls, &process_result(&1, state))
    {:noreply, [], state}
  end

  defp process_result(result, state) do
    case result do
      {:ok, url, body} -> process_html({url, body}, state)
      {:error, url, reason} -> Job.process_error(url, reason, state.job_name)
    end
  end

  defp process_html({url, html}, config) do
    links = Processor.get_links(url, html, config)

    Job.process_success(url, links, config.job_name)
  end
end
