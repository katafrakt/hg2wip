defmodule HunterGatherer.JobState do
  defstruct [:config, urls: %{}, queued: [], processed: 0]

  alias __MODULE__
  @type t :: %__MODULE__{}

  def new(config) do
    %JobState{config: config}
  end

  def add_good(state, url) do
    url_entry =
      state.urls
      |> Map.get(url, %{count: 1})
      |> Map.put(:status, :good)

    queued = List.delete(state.queued, url)

    state
    |> update_url(url, url_entry)
    |> Map.put(:queued, queued)
    |> Map.put(:processed, state.processed + 1)
  end

  def add_bad(state, url, reason) do
    url_entry =
      state.urls
      |> Map.get(url, %{count: 1})
      |> Map.put(:status, :bad)
      |> Map.put(:reason, reason)

    queued = List.delete(state.queued, url)

    state
    |> update_url(url, url_entry)
    |> Map.put(:queued, queued)
    |> Map.put(:processed, state.processed + 1)
  end

  @doc """
  Registers that the URL has been "hit", i.e. found in one of internal processed pages.
  At this point the URL in question might not yet be processed hence its status is unknown.
  """
  @spec mark_hit(JobState.t(), String.t()) :: JobState.t()
  def mark_hit(state, url) do
    url_entry =
      increment_url(state, url)
      |> Map.put_new(:status, :unknown)

    update_url(state, url, url_entry)
  end

  defp increment_url(%{urls: urls}, url) do
    case urls[url] do
      nil -> %{count: 1}
      url -> %{url | count: url.count + 1}
    end
  end

  defp update_url(state, url, entry) do
    urls =
      state.urls
      |> Map.put(url, entry)

    %{state | urls: urls}
  end

  def urls_to_queue(state, urls) do
    urls_to_take = state.config.limit - (state.processed + length(state.queued))

    Enum.reject(urls, fn url ->
      (Map.has_key?(state.urls, url) and state.urls[url].status != :unknown) or
        url in state.queued
    end)
    |> Enum.take(max(0, urls_to_take))
  end

  def queue_urls(state, urls) do
    queued = Enum.uniq(state.queued ++ urls)
    %{state | queued: queued}
  end

  def finished?(state) do
    state.processed >= state.config.limit or
      (Enum.empty?(state.queued) and Enum.any?(state.urls))
  end
end
