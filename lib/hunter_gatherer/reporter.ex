defmodule HunterGatherer.Reporter do
  def render(state, _opts) do
    urls =
      Enum.map(state.urls, fn {key, val} -> Map.put(val, :url, key) end)
      |> Enum.reject(&(&1.status == :unknown))

    good_urls = Enum.count(urls, &(&1.status == :good))

    report = """
    Processed #{state.processed} urls, starting from #{state.config.base_url}.

    #{good_urls} of urls were good.

    """

    urls
    |> Enum.filter(&(&1.status == :bad))
    |> Enum.reduce(report, fn url, acc ->
      acc <> "#{url.url} - #{url.reason} - #{url.count}\n"
    end)
  end
end
