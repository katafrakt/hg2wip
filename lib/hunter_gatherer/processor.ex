defmodule HunterGatherer.Processor do
  @max_redirects 8

  def process_url(url, config), do: process_url(url, config, 0)
  def process_url(_, _, redirect) when redirect >= @max_redirects, do: {:error, :redirect_loop}

  def process_url(url, config, redirect) do
    user_agent = Map.get(config, :user_agent, "HunterGatherer 0.1")

    case HTTPoison.get(url, [{"User-Agent", user_agent}, {"Accept-Encoding", "*"}],
           follow_redirect: true,
           max_redirect: @max_redirects,
           timeout: 30_000,
           recv_timeout: 45_000
         ) do
      {:ok, %{status_code: 200, body: body}} ->
        {:ok, body}

      # handle a non-standard redirect
      {:ok, %{status_code: 308, headers: headers}} ->
        case List.keyfind(headers, "Location", 0) do
          nil -> {:error, 308}
          {_, location} -> process_url(location, config, redirect + 1)
        end

      {:ok, %{status_code: status_code}} ->
        {:error, status_code}

      {:error, %HTTPoison.Error{reason: reason}} ->
        case reason do
          {:invalid_redirection, {:ok, _, headers, _}} ->
            case List.keyfind(headers, "Location", 0) do
              nil -> {:error, 308}
              {_, location} -> process_url(URI.merge(url, location) |> to_string(), config, redirect + 1)
            end

          reason ->
            {:error, reason}
        end

      {:error, error} ->
        {:error, error}
    end
  end

  def get_links(original_url, html, config) do
    if internal?(original_url, config) do
      original_uri = URI.parse(original_url)

      {:ok, document} = Floki.parse_document(html)

      document
      |> Floki.find("a")
      |> Floki.attribute("href")
      |> Enum.map(fn url -> URI.parse(url) end)
      |> Enum.map(fn url ->
        URI.merge(config.base_url, URI.merge(original_uri, url)) |> to_string
      end)
      |> Enum.reject(&(URI.parse(&1).scheme not in ["http", "https"]))
    else
      []
    end
  end

  def internal?(url, config) do
    case String.split(url, config.base_url |> to_string, parts: 2) do
      [_, _] -> true
      _ -> false
    end
  end
end
