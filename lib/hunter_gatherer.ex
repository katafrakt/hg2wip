defmodule HunterGatherer do
  @moduledoc """
  Documentation for `HunterGatherer`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> HunterGatherer.hello()
      :world

  """
  def hello do
    :world
  end
end
