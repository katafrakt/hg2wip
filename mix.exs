defmodule HunterGatherer.MixProject do
  use Mix.Project

  def project do
    [
      app: :hunter_gatherer,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {HunterGatherer.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gen_stage, "~> 1.1"},
      {:floki, "~> 0.32.1"},
      {:httpoison, "~> 1.8"}
    ]
  end
end
